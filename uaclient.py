#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import socket
import hashlib
import os

try:
    CONFIG = sys.argv[1]
    METHOD = sys.argv[2]
    VALOR_SIP = sys.argv[3]
except (ValueError, IndexError, TypeError):
    CONFIG = sys.argv[1]

class ClientHandler(ContentHandler):
    def __init__(self):
        self.etiquetas = []
        self.account = ["username", "passwd"]
        self.uaserver = ["ip", "puerto"]
        self.rtpaudio = ["puerto"]
        self.regproxy = ["ip", "puerto"]
        self.log = ["path"]
        self.audio = ["path"]

    def startElement(self, name, attrs):
        dicc = {}
        if name == 'account':
            for atributo in self.account:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        elif name == 'uaserver':
            for atributo in self.uaserver:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        elif name == 'rtpaudio':
            for atributo in self.rtpaudio:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        if name == 'regproxy':
            for atributo in self.regproxy:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        if name == 'log':
            for atributo in self.log:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        if name == 'audio':
            for atributo in self.audio:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])

    def get_tags(self):
        return dict(self.etiquetas)

def password(passwd, nonce):
    """Devuelve el nonce de respuesta."""
    clave = hashlib.md5()
    clave.update(bytes(passwd, 'utf-8'))
    clave.update(bytes(str(nonce), 'utf-8'))
    return clave.hexdigest()

def log(metodo, address, port, message, fich):
    time_now = time.strftime('%Y%m%d%H%M%S')
    file = open(fich, "a")
    if metodo == 'starting':
        line = (time_now + " Starting...")
    elif metodo == 'send to':
        line = (time_now + " Sent to " + address + ':' + str(port) + ': ' +
                message)
    elif metodo == 'received':
        line = (time_now + " Received from " + address + ':' + str(port) +
                ': ' + message)
    elif metodo == 'error':
        line = (time_now + message)
    elif metodo == 'finishing':
        line = (time_now + " Finishing...")
    file.write(line + '\r\n')
    file.close()

def tenemosError(respuesta):
    if int(respuesta.split()[1]) == 400:
        respuesta_log = (" Error: SIP/2.0 400 Bad Request...")
        print(respuesta_log)
        log('error', None, None, respuesta_log, LOG_FILE)
    elif int(respuesta.split()[1]) == 404:
        respuesta_log = (" Error: SIP/2.0 404 User not found...")
        print(respuesta_log)
        log('error', None, None, respuesta_log, LOG_FILE)
    elif int(respuesta.split()[1]) == 405:
        respuesta_log = (" Error: SIP/2.0 405 Method Not Allowed...")
        print(respuesta_log)
        log('error', None, None, respuesta_log, LOG_FILE)

if __name__ == "__main__":

    parser = make_parser()
    cHandler = ClientHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(CONFIG))
    except FileNotFoundError:
        sys.exit("Usage: python proxy_registrar.py config")
    datosxml = cHandler.get_tags()

    if datosxml['regproxy']['ip'] is None:
        PROXY_SERVER = '127.0.0.1'
    else:
        PROXY_SERVER = datosxml['regproxy']['ip']

    PROXY_PORT = datosxml['regproxy']['puerto']
    USERNAME = datosxml['account']['username']
    PASSWORD = datosxml['account']['passwd']
    IP_UASERVER = datosxml['uaserver']['ip']
    PORT_UASERVER = int(datosxml['uaserver']['puerto'])
    RTPAUDIO = datosxml['rtpaudio']['puerto']
    LOG_FILE = datosxml['log']['path']
    AUDIO_FILE = datosxml['audio']['path']

    """ Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
     del servidor regproxy """
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        my_socket.connect((PROXY_SERVER, int(PROXY_PORT)))

        log('starting', None, None, None, LOG_FILE)

        if METHOD == 'REGISTER':
            SIP = (METHOD + ' sip:' + USERNAME + ':' + str(PORT_UASERVER) +
                   ' SIP/2.0\r\nExpires: ' + VALOR_SIP + '\r\n')
            print('Enviando REGISTER al proxy-registrar...' + '\r\n' + SIP)   
            my_socket.send(bytes(SIP, 'utf-8') + b'\r\n')
            SIP = SIP.replace("\r\n", " ")
            log('send to', PROXY_SERVER, PROXY_PORT, SIP, LOG_FILE)
            data = my_socket.recv(1024).decode('utf-8')
            print('Recibo del proxy-registrar...' + '\r\n' + data)
            data = data.replace("\r\n", " ")
            log('received', PROXY_SERVER, PROXY_PORT, data, LOG_FILE)
            RESPONSE = data.split()
            if RESPONSE[1] == '401':
                NONCE_RECV = RESPONSE[8]
                NONCE = password(PASSWORD, NONCE_RECV)
                SIP = (METHOD + ' sip:' + USERNAME + ':' + str(PORT_UASERVER) +
                       ' SIP/2.0\r\nExpires: ' + VALOR_SIP + '\r\n' +
                       'Authenticate: Digest nonce = ' + NONCE + '\r\n\r\n')
                print('Enviando REGISTER + authorization...' + '\r\n' + SIP)
                my_socket.send(bytes(SIP, 'utf-8') + b'\r\n')
            else:
                tenemosError(data)

        elif METHOD == 'INVITE':
            SIP = (METHOD + ' sip:' + VALOR_SIP + ' SIP/2.0\r\n' +
                   'Content-Type: application/sdp' + '\r\n\r\n' +
                   'v=0\r\n' +
                   'o=' + USERNAME + ' ' + IP_UASERVER + '\r\n' +
                   's=misesion\r\n' +
                   't=0\r\n'
                   'm=audio ' + RTPAUDIO + ' ' + AUDIO_FILE + ' RTP\r\n' +
                   'm=video ' + '23032' + ' ' + 'video' + ' RTP\r\n')

            print('Enviando INVITE al proxy_registrar...' + '\r\n' + SIP)
            my_socket.send(bytes(SIP, 'utf-8') + b'\r\n')
            SIP = SIP.replace("\r\n", " ")
            log('send to', PROXY_SERVER, PROXY_PORT, SIP, LOG_FILE)
            data = my_socket.recv(1024).decode('utf-8')
            print('Recibo del proxy-registrar...' + '\r\n' + data)
            data = data.replace("\r\n", " ")
            log('received', PROXY_SERVER, PROXY_PORT, data, LOG_FILE)

            if data.split()[1] == "100":
                # creo ack una vez que reciba todas las respuestas de INVITE
                SIP = ('ACK sip:' + VALOR_SIP + ' SIP/2.0\r\n')
                print('Enviando ACK al proxy-registrar...' + '\r\n' + SIP)
                my_socket.send(bytes(SIP, 'utf-8') + b'\r\n')
                data = my_socket.recv(1024).decode('utf-8')
                print('Recibo del proxy-registrar...' + '\r\n' + data)
                SIP = SIP.replace("\r\n", " ")
                data = data.replace("\r\n", " ")
                log('send to', PROXY_SERVER, PROXY_PORT, SIP, LOG_FILE)
                log('received', PROXY_SERVER, PROXY_PORT, data, LOG_FILE)
            else:
                tenemosError(data)

        elif METHOD == 'BYE':
            SIP = (METHOD + ' sip:' + VALOR_SIP + ' SIP/2.0\r\n')
            print('Enviando BYE al proxy_registrar...' + '\r\n' + SIP)
            my_socket.send(bytes(SIP, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024).decode('utf-8')
            print('Recibo del proxy-registrar...' + '\r\n' + data)
            SIP = SIP.replace("\r\n", " ")
            log('send to', PROXY_SERVER, PROXY_PORT, SIP, LOG_FILE)
            if data.split()[1] == "200":
                data = data.replace("\r\n", " ")
                log('received', PROXY_SERVER, PROXY_PORT, data, LOG_FILE)
            else:
                tenemosError(data)

        else:
            SIP = (METHOD + ' sip:' + VALOR_SIP + ' SIP/2.0\r\n')
            my_socket.send(bytes(SIP, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024).decode('utf-8')
            print('Recibo del proxy-registrar...' + '\r\n' + data)
            print(data)
            tenemosError(data)

    print("\r\nFin.")
    log('finishing', None, None, None, LOG_FILE)
