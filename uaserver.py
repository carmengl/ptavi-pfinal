#!/usr/bin/python3
# -*- coding: utf-8 -*-import sys

import sys
import os
import threading
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import socketserver
from uaclient import ClientHandler
from uaclient import log

try:
    CONFIG = sys.argv[1]
except (ValueError, IndexError, TypeError):
    sys.exit('Usage: python uaserver.py config ')

class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    def handle(self):
        """ Escribe dirección y puerto del cliente (de tupla client_address)"""
        # Leyendo línea a línea lo que nos envía el cliente
        peticion = self.rfile.read().decode('utf-8')
        print("El proxy-registrar nos manda... " + '\r\n' + peticion)
        METODO = peticion.split()[0]
        log('starting', None, None, None, LOG_FILE)
        peticion = peticion.replace("\r\n", " ")
        log('received', PROXY_SERVER, PROXY_PORT, peticion, LOG_FILE)

        if METODO == 'INVITE':
            respuesta = ('SIP/2.0 100 TRYING...\r\n\r\n' +
                         'SIP/2.0 180 RINGING...\r\n\r\n' +
                         'SIP/2.0 200 OK...\r\n\r\n' +
                         'Content-Type: application/sdp\r\n\r\n' +
                         'v=0\r\n' +
                         'o=' + USERNAME + ' ' + IP_UASERVER + '\r\n' +
                         's=misesion\r\n' +
                         't=0\r\n' +
                         'm=audio ' + RTPAUDIO + ' ' + AUDIO_FILE + ' RTP\r\n' +
                         'm=video ' + '23032' + ' ' + 'video' + ' RTP\r\n' +
                         'Via: SIP/2.0/UDP ' + PROXY_SERVER + ':' + PROXY_PORT)
            self.wfile.write(bytes(respuesta, 'utf-8') + b'\r\n')
            respuesta_log = respuesta.replace("\r\n", " ")
            log('send to', PROXY_SERVER, PROXY_PORT, respuesta_log, LOG_FILE)
        elif METODO == 'ACK':
            # cancion es un string con lo que se ha de ejecutar en la shell
            cancion = ('./mp32rtp -i ' + IP_UASERVER + ' -p ' + RTPAUDIO +
                       ' < ' + AUDIO_FILE)
            cvlc = ('cvlc rtp://@' + IP_UASERVER + ':' + '23032')
            hcvlc = threading.Thread(target=os.system(cvlc + '&'))
            hmp3 = threading.Thread(target=os.system(cancion))
            hcvlc.start()
            hmp3.start()
            print("Vamos a ejecutar:")
            print(cvlc)
            print(cancion)
            log('send to', PROXY_SERVER, PROXY_PORT, cvlc+cancion, LOG_FILE)
        elif METODO == 'BYE':
            self.wfile.write(b"SIP/2.0 200 OK...\r\n\r\n")
            respuesta = ("SIP/2.0 200 OK...")
            log('send to', PROXY_SERVER, PROXY_PORT, respuesta, LOG_FILE)
        elif METODO != ('INVITE', 'BYE', 'ACK'):
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed...\r\n\r\n")
            respuesta = (" Error: SIP/2.0 405 Method Not Allowed...\r\n")
            log('error', None, None, respuesta, LOG_FILE)
        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request...\r\n\r\n")
            respuesta = (" Error: SIP/2.0 400 Bad Request...")
            log('error', None, None, respuesta, LOG_FILE)
        log('finishing', None, None, None, LOG_FILE)

if __name__ == "__main__":
    parser = make_parser()
    cHandler = ClientHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(CONFIG))
    except FileNotFoundError:
        sys.exit("Usage: python proxy_registrar.py config")
    datosxml = cHandler.get_tags()

    if datosxml['uaserver']['ip'] is None:
        IP_UASERVER = '127.0.0.1'
    else:
        IP_UASERVER = datosxml['uaserver']['ip']

    PORT_UASERVER = int(datosxml['uaserver']['puerto'])
    USERNAME = datosxml['account']['username']
    PASSWORD = datosxml['account']['passwd']
    PROXY_SERVER = datosxml['regproxy']['ip']
    PROXY_PORT = datosxml['regproxy']['puerto']
    RTPAUDIO = datosxml['rtpaudio']['puerto']
    LOG_FILE = datosxml['log']['path']
    AUDIO_FILE = datosxml['audio']['path']

    serv = socketserver.UDPServer((IP_UASERVER, int(PORT_UASERVER)), EchoHandler)
    print("Listening...\r\n")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
