#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import socket
import socketserver
import time
import json
import random
import hashlib
from uaclient import log

try:
    CONFIG = sys.argv[1]
except (ValueError, IndexError, TypeError):
    sys.exit('Usage: python proxy_registrar.py config')

class proxyHandler(ContentHandler):
    def __init__(self):
        """  Creacción de varias listas con los datos del fichero xml.  """
        self.etiquetas = []
        self.server = ["name", "ip", "puerto"]
        self.database = ["path", "passwdpath"]
        self.log = ["path"]

    def startElement(self, name, attrs):
        """ Guardamos las listas en un diccionario """
        dicc = {}
        if name == 'server':
            for atributo in self.server:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        elif name == 'database':
            for atributo in self.database:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])
        elif name == 'log':
            for atributo in self.log:
                dicc[atributo] = attrs.get(atributo, "")
            self.etiquetas.append([name, dicc])

    def get_tags(self):
        return dict(self.etiquetas)

def crear_password(passwd, nonce):
    """Devuelve el nonce de respuesta."""
    clave = hashlib.md5()
    clave.update(bytes(str(passwd), 'utf-8'))
    clave.update(bytes(str(nonce), 'utf-8'))
    return clave.hexdigest()

class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """ Echo server class"""

    usuarios = {}
    nonce = {}
    def register2json(self):
        """crea y escribe en el fichero json"""
        with open('registered.json', 'w') as file:
            json.dump(self.usuarios, file, indent=4)

    def json2register(self):
        """si existe el fichero, lo lee su y lo modifica si es necesario"""
        try:
            with open('registered.json', 'r') as archivo_file:
                self.usuarios = json.load(archivo_file)
        except:
            pass

    def registrar(self, users, port, expire_num):
        self.json2register()
        self.usuarios[users] = {'ip': self.client_address[0],
                                'puerto': port,
                                'expires': expire_num}
    def jsonPassword(self):
        """si existe el fichero, lo lee su y lo modifica si es necesario"""
        try:
            with open(PASSWORDPATH, 'r') as password_file:
                self.password = json.load(password_file)
        except (FileNotFoundError, ValueError):
            pass

    def tiempoExpira(self, time_expire):
        """
        method to check expiration of users
        """
        expired_user = []
        current_hour = time.time()
        print(current_hour)
        for user in self.usuarios:
            if self.usuarios[user]["expires"] <= str(current_hour):
                expired_user.append(user)
        for user in expired_user:
            del self.usuarios[user]
        print('Usuarios:', self.usuarios)

    def conexion_server(self, ip_address, puerto_address, method, mensaje):
        # Enviamos lo que recibo al servidor
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # Lo ato al servidor
            my_socket.connect((ip_address, int(puerto_address)))
            print('Reenvio el ' + method + ' al Server...' + '\r\n' + mensaje)
            my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n\r\n')
            mensaje = mensaje.replace("\r\n", " ")
            log('send to', ip_address, puerto_address, mensaje, LOG_PR)
            try:
                data = my_socket.recv(1024)
                recibido = data.decode('utf-8')
                print('\r\nRecibo del servidor...' + '\r\n' + recibido)
                recibido = recibido.replace("\r\n", " ")
                log('received', ip_address, puerto_address, recibido, LOG_PR)
                return(recibido)
            except ConnectionRefusedError:
                respuesta_log = (" Error: No server listening..." +
                                 PROXY_SERVER + ':' + PROXY_PORT + '...')
                log('error', None, None, respuesta_log, LOG_PR)

    def handle(self):
        """ handle method of the server class"""
        solicitud = self.rfile.read().decode('utf-8')
        cabecera_pr = ('Via: SIP/2.0/UDP ' + PROXY_SERVER + ':' + PROXY_PORT +
                       '\r\n')
        peticion = solicitud + cabecera_pr
        print("\r\nEl cliente nos manda..." + '\r\n' + peticion + '\r\n')
        metodo = solicitud.split()[0]
        user = solicitud.split(':')[1]
        ip = self.client_address[0]
        self.jsonPassword()

        log('starting', None, None, None, LOG_PR)
        if metodo == 'REGISTER':
            if len(peticion.split()) == 8:
                self.json2register()
                self.nonce[user] = random.randint(0, 99999999)
                RESPUESTA = ('SIP/2.0 401 Unauthorized\r\n' +
                             'WWW Authenticate: Digest nonce = ' +
                             str(self.nonce[user]) + '\r\n\r\n')
                print('\r\nEnviando al cliente...' + '\r\n' + RESPUESTA)
                self.wfile.write(bytes(RESPUESTA, 'utf-8') + b'\r\n')

            elif len(peticion.split()) > 8:
                self.json2register()
                self.wfile.write(b' SIP/2.0 200 OK\r\n\r\n')
                nonce_recv = peticion.split()[9]
                contraseña = self.password[user]['password']
                print(crear_password(contraseña, nonce_recv))
                expires = peticion.split()[4]
                aux = peticion.split(':')[2]
                puerto = int(aux.split()[0])

                self.registrar(user, puerto, expires)
                print(self.usuarios)

                if user in self.usuarios:
                    if peticion.split()[3] == 'Expires:':
                        if int(expires) == 0:
                            del self.usuarios[user]
                            print('El usuario ha sido eliminado.')
                            print('Usuarios:', self.usuarios)
                            time_exp = time.time() + int(expires)
                            self.tiempoExpira(time_exp)
                else:
                    respuesta = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                    self.wfile.write(bytes(respuesta, 'utf-8') + b'\r\n')
                    respuesta_log = (" Error: SIP/2.0 404 User not found...")
                    log('error', None, None, respuesta_log, LOG_PR)

        elif metodo == 'INVITE':
            aux = peticion.split()[1]
            user = aux.split(':')[1]
            self.json2register()
            if user in self.usuarios:
                puerto = self.usuarios[user]['puerto']
                print('Usuario registrado.')
                recibido = self.conexion_server(ip, puerto, metodo, peticion)
                cabecera_pr = ('Via: SIP/2.0/UDP ' + PROXY_SERVER + ':' +
                               PROXY_PORT + '\r\n')
                recib = recibido + cabecera_pr
                self.wfile.write(bytes(recib, 'utf-8'))
                print('\r\nEnviando al cliente... ' + '\r\n' + recib)
                recibido = recibido.replace("\r\n", " ")
                log('send to', ip, puerto, recib, LOG_PR)
            else:
                respuesta = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                self.wfile.write(bytes(respuesta, 'utf-8') + b'\r\n')
                respuesta_log = (" Error: SIP/2.0 404 User not found...")
                log('error', None, None, respuesta_log, LOG_PR)

        elif metodo == 'ACK':
            aux = peticion.split()[1]
            user = aux.split(':')[1]
            self.json2register()
            if user in self.usuarios:
                puerto = self.usuarios[user]['puerto']
                print('Usuario registrado.')
                recibido = self.conexion_server(ip, puerto, metodo, peticion)
                self.wfile.write(bytes(recibido, 'utf-8'))
                print('\r\nEnviando al cliente... ' + '\r\n' + recibido)
                recibido = recibido.replace("\r\n", " ")
                log('send to', ip, puerto, recibido, LOG_PR)
            else:
                respuesta = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                self.wfile.write(bytes(respuesta, 'utf-8') + b'\r\n')
                respuesta_log = (" Error: SIP/2.0 404 User not found...")
                log('error', None, None, respuesta_log, LOG_PR)

        elif metodo == 'BYE':
            aux = peticion.split()[1]
            user = aux.split(':')[1]
            self.json2register()
            if user in self.usuarios:
                puerto = self.usuarios[user]['puerto']
                print('Usuario registrado.')
                recibido = self.conexion_server(ip, puerto, metodo, peticion)
                self.wfile.write(bytes(recibido, 'utf-8'))
                print('\r\nEnviando al cliente... ' + '\r\n' + recibido)
                recibido = recibido.replace("\r\n", " ")
                log('send to', ip, puerto, recibido, LOG_PR)
            else:
                respuesta = ("SIP/2.0 404 User not found" + "\r\n\r\n")
                self.wfile.write(bytes(respuesta, 'utf-8') + b'\r\n')
                respuesta_log = (" Error: SIP/2.0 404 User not found...")
                log('error', None, None, respuesta_log, LOG_PR)

        elif metodo != ('INVITE', 'BYE', 'ACK'):
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed...\r\n\r\n")
            respuesta_log = (" Error: SIP/2.0 405 Method Not Allowed...")
            log('error', None, None, respuesta_log, LOG_PR)
        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request" + b"\r\n\r\n")
            respuesta = (" Error: SIP/2.0 400 Bad Request...")
            log('error', None, None, respuesta_log, LOG_PR)
        self.register2json()
        log('finishing', None, None, None, LOG_PR)

if __name__ == "__main__":
    parser = make_parser()
    pr_Handler = proxyHandler()
    parser.setContentHandler(pr_Handler)
    try:
        parser.parse(open(CONFIG))
    except FileNotFoundError:
        sys.exit("Usage: python proxy_registrar.py config")
    datosxml = pr_Handler.get_tags()

    if datosxml['server']['ip'] is None:
        PROXY_SERVER = '127.0.0.1'
    else:
        PROXY_SERVER = datosxml['server']['ip']

    PROXY_PORT = datosxml['server']['puerto']
    PASSWORDPATH = datosxml['database']['passwdpath']
    LOG_PR = datosxml['log']['path']
    PROXY_NAME = datosxml['server']['name']
    DATABASE = datosxml['database']['path']

    Handler = SIPRegisterHandler
    serv = socketserver.UDPServer((PROXY_SERVER, int(PROXY_PORT)), Handler)
    print('Server ' + PROXY_NAME + ' listening at port 5555...\r\n')

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
    log('finishing', None, None, None, LOG_PR)
